import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://the-internet.herokuapp.com/')

WebUI.maximizeWindow()

WebUI.verifyElementPresent(findTestObject('Page_The Internet/hompageHeader'), 1)

WebUI.verifyElementClickable(findTestObject('Page_The Internet/menuFileUpload'))

WebUI.click(findTestObject('Page_The Internet/menuFileUpload'))


for (int i = 1; i <= findTestData('Excel/filenames').getRowNumbers(); i++) {
	
	WebUI.verifyElementPresent(findTestObject('Page_The Internet/headerFileUploader'), 1)
	
	WebUI.verifyElementClickable(findTestObject('Page_The Internet/buttonInputFile'))
	
	baseDir = System.getProperty('user.dir')
	
    filename = findTestData('Excel/filenames').getValue(1, i)

    filepath = ((baseDir + '\\Data Files\\Image\\') + filename)

    WebUI.uploadFile(findTestObject('Page_The Internet/buttonInputFile'), filepath)

    WebUI.click(findTestObject('Page_The Internet/buttonSubmitFile'))

    WebUI.verifyElementPresent(findTestObject('Page_The Internet/headerFileUploaded'), 1)

    text = WebUI.getText(findTestObject('Object Repository/Page_The Internet/nameFileUploaded'))

    assert text == filename
	
	WebUI.back()
}

WebUI.closeBrowser()



