<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menuMultipleWindow</name>
   <tag></tag>
   <elementGuidId>8356bd9a-1c8d-4cb3-b62a-ae0bcb9b2d10</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[.='Multiple Windows']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[.='Multiple Windows']</value>
      <webElementGuid>ce812613-6e64-41e2-9d37-aed890eacaa0</webElementGuid>
   </webElementProperties>
</WebElementEntity>
