<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MenuFileDownload</name>
   <tag></tag>
   <elementGuidId>e4d8edde-6628-4862-ae3b-80d8d3a31866</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/ul/li[17]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>62b6dfb5-b97e-4820-97b8-91d0cdb2287b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/download</value>
      <webElementGuid>cd0f3868-7552-435d-ad1f-c8646ca560e4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>File Download</value>
      <webElementGuid>3940f998-7ed0-4007-81fa-a1af17d09800</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/ul[1]/li[17]/a[1]</value>
      <webElementGuid>f769b13f-7d36-499c-9643-e866538437bc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/ul/li[17]/a</value>
      <webElementGuid>5e39c041-2f91-4633-a49b-64228ceaaa23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'File Download')]</value>
      <webElementGuid>cde46616-65df-48ca-b3a2-a8327ea6e2d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Exit Intent'])[1]/following::a[1]</value>
      <webElementGuid>6755a553-436f-41fc-80ec-6ddba51e2efa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Entry Ad'])[1]/following::a[2]</value>
      <webElementGuid>b81a5ba2-235b-4355-86f4-36e6b58774a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='File Upload'])[1]/preceding::a[1]</value>
      <webElementGuid>e3b27bf2-61d1-4fef-9dfb-94c246545c54</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Floating Menu'])[1]/preceding::a[2]</value>
      <webElementGuid>505f7da7-7d01-4cac-a3a3-df4ca9c12bbb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='File Download']/parent::*</value>
      <webElementGuid>87878d72-c8b6-4b9c-8c0e-21582a55017c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/download')]</value>
      <webElementGuid>8d5f6b56-6c1e-46fb-a700-933bdf61230d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[17]/a</value>
      <webElementGuid>88451592-511c-40cf-b8b4-ad99aef7e2a6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/download' and (text() = 'File Download' or . = 'File Download')]</value>
      <webElementGuid>83db46c0-474f-40c3-ba65-b2e17f02b7db</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
