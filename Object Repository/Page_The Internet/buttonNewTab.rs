<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>buttonNewTab</name>
   <tag></tag>
   <elementGuidId>7ae28ca3-1621-4110-bf19-52486c3f8ebd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.example > a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8c7ed7e0-d0d1-40cd-a395-40e0f89a375b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/windows/new</value>
      <webElementGuid>a3f3032f-7b89-484b-97ea-0370849a1901</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>6b122fa0-44e0-4f97-af88-12d11bf815f2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Click Here</value>
      <webElementGuid>e0fbfbf3-8065-4063-b987-4c3c7f32e9d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;example&quot;]/a[1]</value>
      <webElementGuid>d612986c-5a57-4e48-8514-d4abac02a4ff</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/a</value>
      <webElementGuid>22c01f0f-0a47-4424-abc6-583153f43b4c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Click Here')]</value>
      <webElementGuid>7ac29b25-eef5-4a3a-82f0-5c5f4b3c2a32</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Opening a new window'])[1]/following::a[1]</value>
      <webElementGuid>38b5c25d-556f-41f6-86ed-b9614789bda6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elemental Selenium'])[1]/preceding::a[1]</value>
      <webElementGuid>d7633e3c-3d45-47bb-9e26-471f0ebd98f2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Click Here']/parent::*</value>
      <webElementGuid>834eada3-f666-4848-9638-657f949a3042</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/windows/new')]</value>
      <webElementGuid>200107ac-7f08-44dd-b5e9-a33e4208db7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/a</value>
      <webElementGuid>ecf4ef3d-bdb7-489d-8983-b8050cde89d2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/windows/new' and (text() = 'Click Here' or . = 'Click Here')]</value>
      <webElementGuid>06b272aa-4bc3-4887-99e0-86444c4d899c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
