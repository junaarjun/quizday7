<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>headerFileUploader</name>
   <tag></tag>
   <elementGuidId>49468ebc-1bfc-46ce-bdc1-ca1f49d25582</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[.='File Uploader']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[.='File Uploader']</value>
      <webElementGuid>bb7023a6-9a70-431e-8add-ae8974c39c7c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
