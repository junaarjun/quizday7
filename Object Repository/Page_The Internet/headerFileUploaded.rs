<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>headerFileUploaded</name>
   <tag></tag>
   <elementGuidId>9ac2edac-d05f-45e9-aa42-68d835970a2a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div/h3</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h3</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>0204a262-7f77-4e4a-8d81-3df12f029c9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>File Uploaded!</value>
      <webElementGuid>f40a8204-9352-4654-b32f-1d3db98d5acd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;example&quot;]/h3[1]</value>
      <webElementGuid>fb87fb3d-42d7-46fd-9fb1-56a29d9b36a6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div/h3</value>
      <webElementGuid>6eb6dc79-4531-4946-8333-f777abe1b79a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Screenshot_1.png'])[1]/preceding::h3[1]</value>
      <webElementGuid>43bd9d47-bc12-45f0-b690-5ff3e4e9db2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='File Uploaded!']/parent::*</value>
      <webElementGuid>85ad5681-dd48-4708-8bd4-c0764ef8bad6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h3</value>
      <webElementGuid>6bb81ffc-d7db-424f-af62-1760c22592e7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[(text() = 'File Uploaded!' or . = 'File Uploaded!')]</value>
      <webElementGuid>24638f5c-e4e8-4471-bb62-b1c8bbaba5c0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
