<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>headerMultipleWindow</name>
   <tag></tag>
   <elementGuidId>57e1df7d-ad18-4a82-b4f1-568a0a988a14</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//h3[.='Opening a new window']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//h3[.='Opening a new window']</value>
      <webElementGuid>05c656c0-5704-4807-bffa-7fc2699f766c</webElementGuid>
   </webElementProperties>
</WebElementEntity>
